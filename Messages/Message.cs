﻿using MessagePack;
using System;
using System.Collections.Generic;
using System.Text;

namespace Messages
{
    [MessagePackObject]
    [BeetleX.Packets.MessageType(1)]
    public class Message
    {
        [Key(1)]
        public Guid Id { get; set; }
        [Key(2)]
        public string Command { get; set; }
    }

}
