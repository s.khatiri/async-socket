using NUnit.Framework;
using Server;
using Client;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using System;
using System.Linq;

namespace Tests
{
    [TestFixture]
    public class Tests
    {
        private Server.Server _server;
        private int ParallelRequests = 100;
        private int ParallelDegree = 4;
        private List<string> ParallelMessages = new List<string>
        {
            "ping",
            "hello",
            //"bye"
        };

        [SetUp]
        public void Setup()
        {

        }

        [Test]
        public void TestCommands()
        {

            var server = new Server.Server();
            server.Run();
            var client = new Client.Client();

            //ping -> pong
            SendAndTest(server, client, "ping");
            Task.Delay(100).Wait();
            Assert.AreEqual(server._server.Count, 1);

            //bye -> bye , close conection
            SendAndTest(server, client, "bye");
            Task.Delay(100).Wait();
            Assert.AreEqual(server._server.Count, 0);

            //hello -> hi (after 1second delay)
            SendAndTest(server, client, "hello");

        }

        private void SendAndTest(Server.Server server, Client.Client client, string message)
        {
            var stopWatch = new Stopwatch();
            if (message == "hello")
                stopWatch.Start();

            var request=client.SendAsync(message);
            var response = request.Result;

            if (message == "hello")
                stopWatch.Stop();

            switch (message)
            {
                case "ping":
                    Assert.AreEqual("pong", response);
                    break;

                case "bye":
                    Assert.AreEqual("bye", response);
                    break;

                case "hello":
                    Assert.AreEqual("hi", response);
                    //Assert.LessOrEqual(stopWatch.ElapsedMilliseconds, 2000);
                    Assert.LessOrEqual(999, stopWatch.ElapsedMilliseconds);
                    break;

            }

        }

        [Test]
        public void TestParalellRequests()
        {
            var server = new Server.Server();
            server.Run();
            var client = new Client.Client();

            var random = new Random();
            var messages = new List<string>();
            for (var i = 0; i < ParallelRequests ; i++)
            {
                messages.Add(ParallelMessages[random.Next(ParallelMessages.Count)]);
            }
            messages.AsParallel().WithDegreeOfParallelism(ParallelDegree).ForAll(m => SendAndTest(server, client, m));
        }

        [Test]
        public void TestMultipleRequests()
        {
            var server = new Server.Server();
            server.Run();
            var client = new Client.Client();

            var random = new Random();
            var messages = new List<string>();
            for (var i = 0; i < ParallelRequests; i++)
            {
                messages.Add(ParallelMessages[random.Next(ParallelMessages.Count)]);
            }
            var result=messages.Select(m => client.SendAsync(m)).ToList();
        }
        [Test]
        public void TestParalellClients()
        {
            var server = new Server.Server();
            server.Run();

            var random = new Random();
            var messages = new List<string>();
            for (var i = 0; i < ParallelRequests; i++)
            {
                messages.Add(ParallelMessages[random.Next(ParallelMessages.Count)]);
            }

            messages.AsParallel().WithDegreeOfParallelism(ParallelDegree).ForAll(m =>
            {
                var c = new Client.Client();
                SendAndTest(server, c, m);
            });
        }
    }
}