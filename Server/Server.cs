﻿using BeetleX;
using System;
using System.Threading.Tasks;
using Messages;

namespace Server
{
    public class Server : ServerHandlerBase
    {
        public IServer _server;
        public static void Main()
        {
            var server = new Server();
            server.Run();
            Console.ReadLine();
        }

        public void Run(string host = "127.0.0.1", int port = 9090)
        {
            _server = SocketFactory.CreateTcpServer<Server,MsgpackPacket>();
            _server.Options.DefaultListen.Port = port;
            _server.Options.DefaultListen.Host = host;
            _server.Open();
        }
        
        protected override void OnReceiveMessage(IServer server, ISession session, object message)
        {
            //run in background so the requests need not to wait till proccesing ends
            Task.Run(() =>
            {
                var command = (Message)message;

                //Console.WriteLine("server:" + command.Id + "\t" + command.Command);

                switch (command.Command)
                {
                    case "hello":
                        Task.Delay(1000).Wait();
                        command.Command = "hi";
                        server.Send(command, session);
                        break;

                    case "bye":
                        command.Command = "bye";
                        server.Send(command, session);
                        server.CloseSession(session);
                        break;

                    case "ping":
                        command.Command = "pong";
                        server.Send(command, session);
                        break;

                    default:
                        command.Command = "ERROR";
                        server.Send(command, session);
                        break;
                }

            });
        }
    }
}
