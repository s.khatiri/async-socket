﻿using Messages;
using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Client
{
    public class ResponseBuffer
    {
        //used for syncing Requests and their responses
        private readonly ConcurrentDictionary<Guid,BufferItem> _items = new ConcurrentDictionary<Guid, BufferItem>();

        public void AddResponse(Message message)
        {
            try
            {
                if(_items.TryGetValue(message.Id,out BufferItem item))
                {
                    item.Message = message;
                    //set the wait handle so that the waiting request(thread) can go on
                    item.Signal.Set();
                    return;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }

        }

        public async Task<Message> GetResponse(BeetleX.Clients.AsyncTcpClient client, Message message)
        {
            var item = new BufferItem()
            {
                Signal = new ManualResetEventSlim(false),
                Message = null
            };
            
            if (_items.TryAdd(message.Id, item))
            {
                client.Send(message);
                //waits for the response to be ready, the waithandle will be set when reponse is recieved from server
                //item.Signal.Wait(60000);
                await Task.Run(()=>item.Signal.Wait());
                if (_items.TryRemove(message.Id, out BufferItem response))
                {
                    return response.Message;
                }
                throw new Exception("no response");

            }
            throw new Exception("no response");
        }
        public class BufferItem
        {
            public Message Message;
            public ManualResetEventSlim Signal;
        }
    }
}
