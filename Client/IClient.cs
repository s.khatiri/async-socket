﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Client
{
    public interface IClient
    {
        Task<string> SendAsync(string message);
    }
}
