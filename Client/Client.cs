﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;
using BeetleX;
using BeetleX.Clients;
using Messages;

namespace Client
{
    public class Client:IClient
    {
        private readonly AsyncTcpClient _client;
        private readonly ResponseBuffer _buffer=new ResponseBuffer();
        
        static void Main()
        {
            var client = new Client();
            while (true)
            {
                Console.Write("Enter command:");
                var message = Console.ReadLine() ;
                var response=client.SendAsync(message);
                Console.WriteLine(response.Result);
            }
        }
        public Client(string host="127.0.0.1",int port = 9090)
        {
            _client = SocketFactory.CreateClient<AsyncTcpClient,MsgpackClientPacket>(host, port);
            _client.PacketReceive = PacketRecieved;
        }
        private void PacketRecieved(BeetleX.Clients.IClient client,object packet)
        {

            var message = packet as Message;
            _buffer.AddResponse(message);
        }
        public async Task<string> SendAsync(string message)
        {
            try
            {
                var id = Guid.NewGuid();
                var messagePack = new Message()
                {
                    Id = id,
                    Command = message
                };
                var response = await _buffer.GetResponse(_client,messagePack);
                if (response.Command == "ERROR")
                {
                    throw new Exception("invalid message");
                }
                return response.Command;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw e;
            }
        }

    }
}
